import type { NavigationGuardNext, RouteLocationNormalized } from 'vue-router'

import { requiresAuthMiddleware } from './auth'
import { guestMiddleware } from './guest'

export const handleMiddleware = async (
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext,
) => {
  // try {
  //   await oidcStore.loadUser()
  // }
  // catch {}

  const middlewareParams = {
    to,
    from,
    next,
  }

  switch (to.meta.auth) {
    case 'guest':
      return guestMiddleware(middlewareParams)
    case true:
      return requiresAuthMiddleware(middlewareParams)
    default:
      next()
      break
  }
}
