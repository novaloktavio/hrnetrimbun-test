import type { MiddlewareParams } from '@/middleware'

export const requiresAuthMiddleware = async ({
  next,
}: MiddlewareParams) => {
  
  const hasAccess = localStorage.getItem('token');
  if (!hasAccess) {
    next('/login')
  }
  return next()
}
