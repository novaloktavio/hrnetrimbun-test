// Composables
import { createRouter, createWebHistory } from 'vue-router'
import { handleMiddleware } from '@/middleware'

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: '/',
        name: 'Home',
        meta: { auth: true },
        component: () => import('@/views/Home.vue'),
      },
    ],
  },
  {
    path: '/login',
    meta: { auth: 'guest' },
    name: 'Login',
    component: () => import('@/views/login/index.vue'),
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

router.beforeEach(async (to, from, next) => {
  return handleMiddleware(to, from, next)
});


export default router
