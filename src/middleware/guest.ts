import type { MiddlewareParams } from '@/middleware'

export const guestMiddleware = async ({next}: MiddlewareParams) => {
  if (localStorage.getItem('token')) {
    next('/')
  }
  return next()
}
