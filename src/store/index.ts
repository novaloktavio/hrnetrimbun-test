// Utilities
import { createPinia } from 'pinia'

export { useAuthStore } from './auth'
export default createPinia()
