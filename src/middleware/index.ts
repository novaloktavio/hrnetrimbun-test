import type { RouteLocationNormalized, NavigationGuardNext } from 'vue-router'

export interface MiddlewareParams {
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext,
}

export * from './auth'
export * from './guest'
export * from './handler'
