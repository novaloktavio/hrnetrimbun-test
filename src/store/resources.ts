import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useResourcesStore = defineStore('resources', () => {
  const items = ref([
    {
      type: 'Room',
      name: 'Meeting Room 1',
      desc: 'A small meeting room for max 5 person.',
      is_active: true,
      location: 'Floor 1',
      cost: 5000000,
      last_status_checked: 'Good Condition',
    },
    {
      type: 'Desk',
      name: 'Desk 180',
      desc: 'A standard office desk with a computer.',
      is_active: false,
      location: 'Floor 1',
      cost: 1500000,
      last_status_checked: 'Good Condition',
    },
    {
      type: 'Monitor',
      name: 'LG Monitor 24"',
      desc: 'A widescreen monitor for better productivity.',
      is_active: true,
      location: 'Floor 2',
      cost: 1500000,
      last_status_checked: 'Good Condition',
    },
  ])
  function addNew(item: any) {
    items.value.push(item)
  }

  return { items, addNew }
})
