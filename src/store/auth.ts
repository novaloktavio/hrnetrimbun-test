// Utilities
import { defineStore } from 'pinia'

export const useAuthStore = defineStore('authStore', {
  state: () => ({
    user: null,
    token: false
  }),
  actions: {
    login(user: any) {
      if (user) {
        this.setUser(user)
      }
    },
    logout() {
      this.unsetUser();
    },
    setUser(user: any) {
      if (!user)
        return

      this.user = user
      this.token = !!user
      localStorage.setItem('token', String(this.token));

    },
    unsetUser() {
      this.user = null
      this.token = false
      localStorage.removeItem('token');
    },
  },
})
